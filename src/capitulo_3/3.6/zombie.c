#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
int main()
{
    pid_t child_pid;
    /* Create a child process. */

    child_pid = fork();
    if (child_pid > 0)
    {
        /* This is the parent process. Sleep for a minute. */
        printf("\n Proceso padre durmiendo \n");
        sleep(60);
    }
    else
    {
        printf("\n Proceso hijo termina. Proceso zombie creado. Verificar con  $ ps -e -o pid,ppid,stat,cmd \n");
        /* This is the child process. Exit immediately. */
        exit(0);
    }

    return 0;
}