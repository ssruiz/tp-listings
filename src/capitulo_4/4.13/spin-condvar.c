#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
int thread_flag;
pthread_mutex_t thread_flag_mutex;
void initialize_flag()
{
    pthread_mutex_init(&thread_flag_mutex, NULL);
    thread_flag = 0;
}
/* Calls do_work repeatedly while the thread flag is set; otherwise
spins. */
/* Sets the value of the thread flag to FLAG_VALUE. */

void set_thread_flag(int flag_value)
{
    printf("\nCambiando variable de condición a -> %d  \n", flag_value);
    /* Protect the flag with a mutex lock. */
    pthread_mutex_lock(&thread_flag_mutex);
    thread_flag = flag_value;
    pthread_mutex_unlock(&thread_flag_mutex);
}

void do_work()
{
    printf("\nDoing job");
    set_thread_flag(0);
    printf("\nVolviendo a ciclo infinito job");
}
void *thread_function(void *thread_arg)
{
    printf("\nEsperando que variable de condición se active %d", thread_flag);

    while (1)
    {
        int flag_is_set;
        /* Protect the flag with a mutex lock. */
        pthread_mutex_lock(&thread_flag_mutex);
        flag_is_set = thread_flag;
        pthread_mutex_unlock(&thread_flag_mutex);
      //  printf("ID == %d", flag_is_set);
        if (flag_is_set)
            do_work();
        /* Else don’t do anything. Just loop again. */
    }
    //        set_thread_flag(1);
    return NULL;
}

void *funcion_aux(void *arg)
{
  
    set_thread_flag(1);
    return NULL;
}

int main()
{
    pthread_t thr1, thr2;

    pthread_create(&thr1, NULL, &thread_function, NULL);

    pthread_create(&thr2, NULL, &funcion_aux, NULL);

    pthread_join(thr1, NULL);

    pthread_join(thr2, NULL);
}
