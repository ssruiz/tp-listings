#include <stdio.h>

#include <stdlib.h>

#include <malloc.h>

#include <pthread.h>
#include <unistd.h>
struct job

{

    struct job *next;

    int value;
};

struct job *job_queue;

void process_job(struct job *);

pthread_mutex_t job_queue_mutex = PTHREAD_MUTEX_INITIALIZER;

void *process_queue_function(void *arg)

{
    int i = 0;
    while (1)

    {
       
        struct job *next_job;
        pthread_mutex_lock(&job_queue_mutex);
 
        if (job_queue == NULL)

            next_job = NULL;

        else

        {

            //printf("Empezando a remover el job...\n");

            next_job = job_queue;

            job_queue = job_queue->next;

            //printf("Despues de remover el job...\n");
        }

        pthread_mutex_unlock(&job_queue_mutex);

        if (next_job == NULL )

        {

            sleep(5);

            break;
        }

        process_job(next_job);

        free(next_job);
    }

    return NULL;
}

void process_job(struct job *p)

{

    printf("Job %d procesado desde ThreadID=  %d.\n", p->value , pthread_self());
}

void enqueue_job(struct job *new_job)

{

    pthread_mutex_lock(&job_queue_mutex);

    new_job->next = job_queue;

    job_queue = new_job;

    printf("Job insertado...\n");

    pthread_mutex_unlock(&job_queue_mutex);
}

void insert_queue_function()

{

    int i = 0;

    while (i < 10)

    {

        sleep(1);

        printf("Ingresa el valor: %d.\n", i);

        struct job *new_job = (struct job *)malloc(sizeof(struct job));

        new_job->next = NULL;

        new_job->value = i;

        enqueue_job(new_job);

        i++;
    }
}

int main()

{
    insert_queue_function();
    pthread_t insert_thread, process_thread;

    pthread_create(&insert_thread, NULL, &process_queue_function, NULL);

    pthread_create(&process_thread, NULL, &process_queue_function, NULL);

    pthread_join(insert_thread, NULL);

    pthread_join(process_thread, NULL);
}