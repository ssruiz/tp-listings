#include <stdio.h>

#include <stdlib.h>

#include <pthread.h>

class ThreadExitException

{

public:
    ThreadExitException(void *return_value) : thread_return_value(return_value) {}

    void *DoThreadExit()

    {

        pthread_exit(thread_return_value);
    }

private:
    void *thread_return_value;
};

class example

{

public:
    example(int size)

    {

        printf("Asignando recursos...");

        memory = malloc(size);
    }

    ~example()

    {

        printf("liberando buffer...");

        free(memory);
    }

private:
    void *memory;
};

void do_some_work()

{

    printf("do some work...");

    example e(100);

    throw ThreadExitException(NULL);
}

void *thread_function(void *)

{

    try

    {

        do_some_work();
    }

    catch (ThreadExitException ex)

    {

        ex.DoThreadExit();
    }

    return NULL;
}

int main()

{

    pthread_t thread_id;

    pthread_create(&thread_id, NULL, &thread_function, NULL);

    pthread_join(thread_id, NULL);
}