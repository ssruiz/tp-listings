#include <pthread.h>
#include <stdio.h>
void *thread_function(void *thread_arg)
{
    /* Do work here... */
    int i = 0;
    while (i < 10)
    {
        printf("\nContando desde hilo hijo: %d\n", i);
        i++;
    }
}
int main()
{
    pthread_attr_t attr;
    pthread_t thread;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_create(&thread, &attr, &thread_function, NULL);
    pthread_attr_destroy(&attr);
    /* Do work here... */

    int i = 0;
    while (i < 11)
    {
        printf("\nContando desde hilo padre: %d\n", i);
        i++;
    }
    /* No need to join the second thread. */
    return 0;
}