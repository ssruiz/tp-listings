#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
// estructura para la transaccion
struct valores_transaccion
{
    //dinero a transferir
    float money;
    // cuenta de la que sacar el dinero
    int account1;
    // cuenta a la adjudicar el dinero
    int account2;
};

/* An array of balances in accounts, indexed by account number. */
float *account_balances;
/* Transfer DOLLARS from account FROM_ACCT to account TO_ACCT. Return
0 if the transaction succeeded, or 1 if the balance FROM_ACCT is
too small. */
void *process_transaction(void *parameters)
{
    int old_cancel_state;
    struct valores_transaccion *p = (struct valores_transaccion *)parameters;
    /* Check the balance in FROM_ACCT. */
    if (account_balances[p->account1] < p->money)
        return (void *)1;
    /* Begin critical section. */
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &old_cancel_state);
    /* Move the money. */
    account_balances[p->account2] += p->money;
    account_balances[p->account1] -= p->money;
    /* End critical section. */
    pthread_setcancelstate(old_cancel_state, NULL);
    return (void *)0;
}

int main()
{
    pthread_t thread;
    struct valores_transaccion argumentos;

    argumentos.account1 = 0;
    argumentos.account2 = 1;
    argumentos.money = 2000;

    float accounts[2];
    account_balances = accounts;
    account_balances[0] = (float)5000;
    account_balances[1] = (float)3000;

    printf("Valores iniciales de las cuentas\n");
    printf("\nCuenta 1: %.2f", account_balances[0]);
    printf("\nCuenta 2: %.2f", account_balances[1]);
    printf("\nDinero a transferir de 1 a 2: %.2f\n", argumentos.money);
    printf("\nCreando hilo para la transacción \n");

    pthread_create(&thread, NULL, &process_transaction, &argumentos);

    printf("\nIntentando cancelar el hilo de la transacción \n");
    pthread_cancel(thread);
    sleep(2);
    /* Start the computing thread, up to the 5,000th prime number. */
    int r;
    pthread_join(thread, (void *)&r);
    printf("\nResultado de la transacción (0 = exitosa ; != 0 error) = %d\n", r);

    printf("Valores luegos de la transacción\n");
    printf("\nCuenta 1: %.2f", account_balances[0]);
    printf("\nCuenta 2: %.2f", account_balances[1]);
    printf("\nDinero transferido de 1 a 2: %.2f\n", argumentos.money);
    printf("\nCreando hilo para la transacción \n");
    return 0;
}