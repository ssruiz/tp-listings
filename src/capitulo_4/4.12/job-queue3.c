#include <stdio.h>

#include <stdlib.h>

#include <malloc.h>

#include <pthread.h>

#include <semaphore.h>
#include <unistd.h>
struct job

{

    struct job *next;

    int value;
};

struct job *job_queue;

void process_job(struct job *);

pthread_mutex_t job_queue_mutex = PTHREAD_MUTEX_INITIALIZER;

sem_t job_queue_count;

void initialize_job_queue()

{

    job_queue = NULL;

    sem_init(&job_queue_count, 0, 0);
}

void *process_queue_function(void *arg)

{

    while (1)

    {

        struct job *next_job;

        sem_wait(&job_queue_count);

        pthread_mutex_lock(&job_queue_mutex);

        printf("begin removing a job...\n");

        next_job = job_queue;

        job_queue = job_queue->next;

        printf("after removing a job...\n");

        pthread_mutex_unlock(&job_queue_mutex);

        process_job(next_job);

        free(next_job);
    }

    return NULL;
}

void process_job(struct job *p)

{

    printf("The value is : %d.\n", p->value);
}

void enqueue_job(struct job *new_job)

{

    pthread_mutex_lock(&job_queue_mutex);

    printf("begin inserting a job...\n");

    new_job->next = job_queue;

    job_queue = new_job;

    sem_post(&job_queue_count);

    printf("after inserting a job...\n");

    pthread_mutex_unlock(&job_queue_mutex);
}

void *insert_queue_function(void *arg)

{

    int i = 0;

    while (i < 20)

    {

        sleep(1);

        printf("put the value: %d.\n", i);

        struct job *new_job = (struct job *)malloc(sizeof(struct job));

        new_job->next = NULL;

        new_job->value = i;

        enqueue_job(new_job);

        i++;
    }
}

int main()

{

    pthread_t insert_thread, process_thread;

    pthread_create(&insert_thread, NULL, &insert_queue_function, NULL);

    pthread_create(&process_thread, NULL, &process_queue_function, NULL);

    pthread_join(insert_thread, NULL);

    pthread_join(process_thread, NULL);
}