#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

struct job
{
    /* Link field for linked list. */
    int valor;
    struct job *next;
    /* Other fields describing work to be done... */
};
/* A linked list of pending jobs. */
struct job *job_queue;
/* Process queued jobs until the queue is empty. */

void procces_job(struct job *do_job)
{
    printf("\nRealizando job %d desde Thread: %d", do_job->valor, pthread_self());
}



    void *thread_function(void *arg)
{
    while (job_queue != NULL)
    {
        /* Get the next available job. */
        struct job *next_job = job_queue;
        /* Remove this job from the list. */
        job_queue = job_queue->next;
        /* Carry out the work. */
        procces_job(next_job);
        /* Clean up. */
        free(next_job);
    }
    return NULL;
}



void insertar_jobs()

{

    int i = 0;

    while (i < 10)

    {

        sleep(1);

        printf("Inserta el job: %d.\n", i);

        struct job *new_job = (struct job *)malloc(sizeof(struct job));

        new_job->next = NULL;

        new_job->valor = i;

        new_job->next = job_queue;

        job_queue = new_job;

        i++;
    }
}

int main(){

    insertar_jobs();

    pthread_t t1, t2;

    printf("\nSe crean dos hilos y se procesan los jobs\n");
    pthread_create(&t1, NULL, &thread_function, NULL);

    pthread_create(&t2, NULL, &thread_function, NULL);

    pthread_join(t1, NULL);

    pthread_join(t2, NULL);

    return 0;
}